DcsTools
========

A Symfony project created on March 6, 2018, 11:11 pm.

Requirements to work on this project:
- docker
- git
- make
- mysql database

[features details here](doc/features.md)

To work on this project
-----------------------

```shell
# clone the repository
git clone git@git.kilik.fr:veaf/dcs/tools.git
cd tools
# prepare default setup (works with a reverse proxy, see VIRTUAL_HOST)
cp .env.dist .env
# build the pod
make build
# up the pod
make up
# enter an interactive container
make shell
```

In the php container

```shell
# install back dependencies
composer install
# install front dependencies
yarn install
```

To access the project: http://dcstools.localhost/app_dev.php