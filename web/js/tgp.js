$(function () {

    'use strict';

    var console = window.console || { log: function () {} };
    var URL = window.URL || window.webkitURL;
    var $image = $('#image');
    var $download = $('#download');
    var $dataX = 0;
    var $dataY = 0;
    var $dataHeight = 1000;
    var $dataWidth = 1000;
    var $dataRotate = 0;
    var $dataScaleX = 1;
    var $dataScaleY = 1;
    var options = {
        aspectRatio: 1,
        preview: '.img-preview',
        crop: function (e) {
            console.log(e);

            $dataX=Math.round(e.x);
            $dataY=Math.round(e.y);
            $dataHeight=Math.round(e.height);
            $dataWidth=Math.round(e.width);
            $dataRotate=e.rotate;
            $dataScaleX=e.scaleX;
            $dataScaleY=e.scaleY;
        }
    };
    var originalImageURL = $image.attr('src');
    var uploadedImageName = 'cropped.jpg';
    var uploadedImageType = 'image/jpeg';
    var uploadedImageURL;


    // Tooltip
    $('[data-toggle="tooltip"]').tooltip();


    // Cropper
    $image.on({
        ready: function (e) {
            console.log(e.type);
        },
        cropstart: function (e) {
            console.log(e.type, e.detail.action);
        },
        cropmove: function (e) {
            console.log(e.type, e.detail.action);
        },
        cropend: function (e) {
            console.log(e.type, e.detail.action);
        },
        crop: function (e) {
            console.log(e.type);
        },
        zoom: function (e) {
            console.log(e.type, e.detail.ratio);
        }
    }).cropper(options);


    // Buttons
    if (!$.isFunction(document.createElement('canvas').getContext)) {
        $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
    }

    if (typeof document.createElement('cropper').style.transition === 'undefined') {
        $('button[data-method="rotate"]').prop('disabled', true);
        $('button[data-method="scale"]').prop('disabled', true);
    }

    $('#btnCrop').click(function () {
        var options={
            dataX: $dataX,
            dataY: $dataY,
            dataHeight: $dataHeight,
            dataWidth: $dataWidth,
            dataScaleX: $dataScaleX,
            dataScaleY: $dataScaleY,
        };
        var result = $image.cropper('getCroppedCanvas', options);
        console.log(options);
        $("#imageCropped").attr('src', result.toDataURL(uploadedImageType));
        refreshTgp();
        //$download.attr('href', result.toDataURL(uploadedImageType));

        //console.log($image.cropper('getCroppedCanvas'));
        //console.log(cropper('getCroppedCanvas'));
    });


    // Import image
    var $inputImage = $('#inputImage');

    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    uploadedImageName = file.name;
                    uploadedImageType = file.type;

                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    uploadedImageURL = URL.createObjectURL(file);
                    $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                    $inputImage.val('');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }

});