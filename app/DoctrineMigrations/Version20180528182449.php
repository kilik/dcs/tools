<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180528182449 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE miz ADD blue_uuid VARCHAR(255) NOT NULL, ADD red_uuid VARCHAR(255) NOT NULL');
        $this->addSql('UPDATE miz SET blue_uuid = (SELECT UUID()), red_uuid = (SELECT UUID())');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BBA1C9ADBFC9BEBD ON miz (blue_uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BBA1C9AD8BC4B02A ON miz (red_uuid)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_BBA1C9ADBFC9BEBD ON miz');
        $this->addSql('DROP INDEX UNIQ_BBA1C9AD8BC4B02A ON miz');
        $this->addSql('ALTER TABLE miz DROP blue_uuid, DROP red_uuid');
    }
}
