<?php

namespace AppBundle\Entity;

use AppBundle\Components\Dictionary;
use AppBundle\Components\Mission;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Miz
 *
 * @ORM\Table(name="miz")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MizRepository")
 */
class Miz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Mission make UUID
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, unique=true)
     */
    private $uuid;

    /**
     * Blue side UUID
     *
     * @var string
     *
     * @ORM\Column(name="blue_uuid", type="string", length=255, unique=true)
     */
    private $blueUuid;

    /**
     * Red side UUID
     *
     * @var string
     *
     * @ORM\Column(name="red_uuid", type="string", length=255, unique=true)
     */
    private $redUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var array
     *
     * @ORM\Column(name="mission", type="json_array")
     */
    private $originalMission;

    /**
     * @var array
     *
     * @ORM\Column(name="dictionary", type="json_array")
     */
    private $dictionary;

    /**
     * Mission (cache)
     *
     * @var
     */
    private $mission;

    /**
     * @var string
     */
    private $side;

    /**
     * Miz constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return Miz
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $blueUuid
     *
     * @return Miz
     */
    public function setBlueUuid($blueUuid)
    {
        $this->blueUuid = $blueUuid;

        return $this;
    }

    /**
     * @return string
     */
    public function getBlueUuid()
    {
        return $this->blueUuid;
    }

    /**
     * @param string $redUuid
     *
     * @return Miz
     */
    public function setRedUuid($redUuid)
    {
        $this->redUuid = $redUuid;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedUuid()
    {
        return $this->redUuid;
    }

    /**
     * @param string $title
     *
     * @return Miz
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @ORM\PrePersist
     */
    public function setNewUuid()
    {
        if (null === $this->uuid) {
            $this->uuid = (string)(Uuid::uuid4());
        }
        if (null === $this->blueUuid) {
            $this->blueUuid = (string)(Uuid::uuid4());
        }
        if (null === $this->redUuid) {
            $this->redUuid = (string)(Uuid::uuid4());
        }
    }

    /**
     * @param array $mission
     */
    public function setOriginalMission(array $mission)
    {
        $this->originalMission = $mission;
    }

    /**
     * @return array
     */
    public function getOriginalMission()
    {
        return $this->originalMission;
    }

    /**
     * @param array $dictionary
     *
     * @return Miz
     */
    public function setDictionary($dictionary)
    {
        $this->dictionary = $dictionary;

        return $this;
    }

    /**
     * @return array
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * @todo persist this data to add custom additional data
     *
     * @return Mission
     */
    public function getMission()
    {
        if (null === $this->mission) {
            $this->mission = Mission::buildFromArray($this->getOriginalMission(), Dictionary::buildFromArray($this->getDictionary()));
        }

        return $this->mission;
    }

    /**
     * @return string
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * @param string $side
     */
    public function setSide($side)
    {
        $this->side = $side;
    }

    /**
     * get the uuid switch side
     *
     * @param string $side
     *
     * @return string
     */
    public function getUuidSide($side)
    {
        switch ($side) {
            case 'red':
                return $this->redUuid;
                break;
            case 'blue':
                return $this->blueUuid;
                break;
            case 'mission_maker':
            default:
                return $this->uuid;
                break;

        }

    }

}

