<?php

namespace AppBundle\Services;

use AppBundle\Components\Waypoint;
use AppBundle\Entity\Miz;
use AppBundle\Lib\LuaParser;

class MizService
{
    /**
     * @var string
     */
    private $kernelDir;

    /**
     * MizService constructor.
     *
     * @param string $kernelDir
     */
    public function __construct($kernelDir)
    {
        $this->kernelDir = $kernelDir;
    }

    /**
     * Get MIZ Base Path
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->kernelDir.'/../var/data';
    }

    /**
     * Get MIZ Data Path
     *
     * @param Miz $mission
     *
     * @return string
     */
    public function getDataPath(Miz $mission)
    {
        if (null === $mission->getUuid() || '' === $mission->getUuid()) {
            throw new \InvalidArgumentException('mission has no uuid');
        }

        return $this->getBasePath().'/'.$mission->getUuid();
    }

    /**
     * Create a MIZ from a file (path)
     *
     * @param string $mizFilePath
     *
     * @throws \Exception
     *
     * @return Miz
     */
    public function createFromMizFile($mizFilePath)
    {
        $zip = new \ZipArchive();
        if ($zip->open($mizFilePath) == false) {
            throw new \Exception('error opening MIZ file');
        }
        $lua = $zip->getFromName('mission');
        $parser = new LuaParser();
        $parser->loadContent($lua);
        $result = $parser->parse();
        if (!is_array($result) || !isset($result ['mission'])) {
            throw new \Exception('mission not found in ZIP file');
        }

        $mission = $result['mission'];

        $luaDictionary = $zip->getFromName('l10n/DEFAULT/dictionary');
        $parser = new LuaParser();
        $parser->loadContent($luaDictionary);
        $result = $parser->parse();
        if (!is_array($result) || !isset($result ['dictionary'])) {
            throw new \Exception('dictionary not found in ZIP file');
        }
        $dictionary = $result['dictionary'];

        $miz = new Miz();
        $miz->setNewUuid();
        $miz->setOriginalMission($mission);
        $miz->setTitle('mission imported '.date('Y-m-d H:i'));
        $miz->setDictionary($dictionary);

        // copy miz
        if (!is_dir($this->getDataPath($miz))) {
            mkdir($this->getDataPath($miz), 0777, true);
        }
        if (!is_dir($this->getDataPath($miz).'/src')) {
            mkdir($this->getDataPath($miz).'/src');
        }
        // copy miz file
        copy($mizFilePath, $this->getDataPath($miz).'/mission.miz');

        return $miz;
    }

    /**
     * Translate a text
     *
     * @param Miz $miz
     * @param string $key
     *
     * @return string
     * @deprecated since Mission Object
     */
    public function trans(Miz $miz, $key)
    {
        $dictionary = $miz->getDictionary();
        if (isset($dictionary[$key])) {
            return $dictionary[$key];
        } else {
            return $key;
        }
    }

    /**
     * Count units by type (from a group)
     *
     * @param Unit[] $units
     *
     * @return array
     */
    public function countUnitsByType($units)
    {
        $sum = [];
        foreach ($units as $unit) {
            if (!isset($sum[$unit->getType()])) {
                $sum[$unit->getType()] = 1;
            } else {
                $sum[$unit->getType()]++;
            }
        }

        ksort($sum);

        return $sum;
    }

    /**
     * Find next waypoint
     *
     * @param array $waypoints
     * @param int $waypointId
     *
     * @return array|null
     */
    public function nextWaypoint($waypoints, $waypointId)
    {
        if (isset($waypoints[$waypointId + 1])) {
            return $waypoints[$waypointId + 1];
        }

        return null;
    }

    /**
     * Calculate distance and heading from waypoint to next waypoint
     *
     * @param Waypoint $waypoints []
     * @param int $waypointId
     *
     * @return null|array
     * @deprecated since Mission Object
     */
    public function calcNextWaypoint($waypoints, $waypointId)
    {
        /** @var Waypoint $waypoint */
        $waypoint = $waypoints[$waypointId];
        /** @var Waypoint $nextWaypoint */
        $nextWaypoint = $this->nextWaypoint($waypoints, $waypointId);
        if (null === $nextWaypoint) {
            return null;
        }

        // distance in meters
        $distance = $this->calcDistance($waypoint, $nextWaypoint);
        $ETA = 0;
        // speed in m/s
        if ($waypoint->getSpeed() > 0) {
            $ETA = $distance / $waypoint->getSpeed();
        }

        return [
            'distance' => $distance,
            'heading' => $this->calcHeading($waypoint, $nextWaypoint),
            'speed' => $waypoint->getSpeed(),
            'ETA' => $ETA,
        ];
    }

    /**
     * Calculate distance between 2 waypoints
     *
     * @param Waypoint $from
     * @param Waypoint $to
     *
     * @return float distance
     */
    public function calcDistance(Waypoint $from, Waypoint $to)
    {
        $dx = $to->getPosition()->getX() - $from->getPosition()->getX();
        $dy = $to->getPosition()->getY() - $from->getPosition()->getY();
        $d = sqrt($dx * $dx + $dy * $dy);

        return $d;
    }

    /**
     * Calculate HDG between 2 points
     *
     * @param Waypoint $from
     * @param Waypoint $to
     *
     * @return float heading
     */
    public function calcHeading(Waypoint $from, Waypoint $to)
    {
        $dx = $to->getPosition()->getX() - $from->getPosition()->getX();
        $dy = $to->getPosition()->getY() - $from->getPosition()->getY();
        $rad = atan2($dy, $dx);
        $deg = rad2deg($rad);
        if ($deg < 0) {
            $deg += 360;
        }

        return round($deg);
    }

    /**
     * Get Groups And Units (without country)
     * @deprecated since Mission Object
     */
    public function getGroupsAndUnits(Miz $miz, $coalition)
    {
        $types = ['plane' => [], 'helicopter' => [] /*, 'vehicle' */];

        foreach ($coalition['country'] as $countryId => $countryInfos) {
            foreach ($countryInfos as $vehicleType => $vehicleInfos) {
                if (isset($types[$vehicleType])) {
                    if (isset($vehicleInfos['group'])) {
                        foreach ($vehicleInfos['group'] as $group) {
                            $types[$vehicleType][] = $group;
                        }
                    }
                }
            }
        }

        foreach ($types as $type => $groups) {
            usort(
                $groups,
                function ($a, $b) use ($miz) {
                    $nameA = $this->trans($miz, $a['name']);
                    $nameB = $this->trans($miz, $b['name']);

                    return $nameA >= $nameB;
                }
            );
            $types[$type] = $groups;
        }

        return $types;
    }

    /**
     * Get map range
     *
     * @return array [(x1,y1),(x2,y2)]
     * @deprecated since openlayers
     */
    public function getMapRange(Miz $miz)
    {
        $mission = $miz->getMission();

        $min = null;
        $max = null;

        //dump($miz->getMission());
        foreach ($miz->getMission()['coalition'] as $coaltionId => $coaltionInfos) {
            foreach ($coaltionInfos['country'] as $countryId => $countryInfos) {
                foreach ($countryInfos as $vehicleType => $vehicleInfos) {
                    if (isset($vehicleInfos['group'])) {
                        foreach ($vehicleInfos['group'] as $group) {
                            if (isset($group['route'])) {
                                foreach ($group['route'] as $route) {
                                    if (is_array($route)) {
                                        foreach ($route as $waypoint) {
                                            if (isset($waypoint['x']) && isset($waypoint['y'])) {
                                                //dump($waypoint);
                                                if (null === $min) {
                                                    $min = [
                                                        'x' => $waypoint['x'],
                                                        'y' => $waypoint['y'],
                                                    ];
                                                } elseif ($min['x'] > $waypoint['x']) {
                                                    $min['x'] = $waypoint['x'];
                                                } elseif ($min['y'] > $waypoint['y']) {
                                                    $min['y'] = $waypoint['y'];
                                                }

                                                if (null === $max) {
                                                    $max = [
                                                        'x' => $waypoint['x'],
                                                        'y' => $waypoint['y'],
                                                    ];
                                                } elseif ($max['x'] < $waypoint['x']) {
                                                    $max['x'] = $waypoint['x'];
                                                } elseif ($max['y'] < $waypoint['y']) {
                                                    $max['y'] = $waypoint['y'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        return ['min' => $min, 'max' => $max, 'dim' => ['x' => $max['x'] - $min['x'], 'y' => $max['y'] - $min['y']]];
    }

    /**
     * Get canvas scale
     *
     * @param array $mapRange
     * @param array $canvasDimension
     *
     * @return float
     * @deprecated since openlayers
     */
    public function getCanvasScale($mapRange, $canvasDimension)
    {
        return $canvasDimension['x'] / $mapRange['dim']['x'];
    }

    /**
     * Get canvas position from range
     *
     * @param array $mapRange
     * @param array $canvasDimension
     * @param array $point ['x','y']
     *
     * @return array ['x','y']
     *
     * @deprecated since openlayers
     */
    public function getCanvasPosition($mapRange, $canvasDimension, $point)
    {
        $px = round(($mapRange['max']['x'] - $point['x']) * $canvasDimension['x'] / $mapRange['dim']['x']);
        $py = round(($point['y'] - $mapRange['min']['y']) * $canvasDimension['y'] / $mapRange['dim']['y']);

        // swap x and y
        return ['x' => $py, 'y' => $px];
    }

}