<?php

namespace AppBundle\Services;

use AppBundle\Components\Position;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

use proj4php\Proj4php;
use proj4php\Proj;
use proj4php\Point;

class ProjectionService
{
    /** @var Proj4php */
    private $proj4;

    public function __construct()
    {
        $this->proj4 = new Proj4php();
    }

    /**
     * @param Position $p
     * @param string $map
     * @return string
     */
    public function xyToLL(Position $p, $map = "caucasus")
    {
        $projSource = new Proj('EPSG:4326', $this->proj4);

        // source VEAF Magic:
        //        point (0,0)
        //        DD : 34.26551518845619,45.12949706032199
        //        DMS: 045°07′46.189″N  034°15′55.855″E
        //        point (0,500K)
        //        DD : 40.59116773673568,44.88347735909489
        //        DMS: 044°53′00.518″N  040°35′28.204″E
        //        point (-500K,500K)
        //        DD : 40.06378105888497,40.4174184154516
        //        DMS: 040°25′02.706″N  040°03′49.612″E

        switch(strtolower($map)) {
            case "persiangulf":
                $projDestination = new Proj('+proj=tmerc +lat_0=26.1718 +lon_0=56.2419 +k_0=0.9996 +x_0=0 +y_0=0', $this->proj4);
                break;
            case "caucasus":
            default:
            $projDestination = new Proj('+proj=tmerc +lat_0=0 +lon_0=33 +k_0=0.9996 +x_0=-99517 +y_0=-4998115', $this->proj4);
        }

        // need to swap X and Y
        $source = new Point($p->getY(), $p->getX(), null, $projDestination);
        $destination = $this->proj4->transform($projSource, $source);

        return new Position($destination->x, $destination->y);
    }

}