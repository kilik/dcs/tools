<?php

namespace AppBundle\Command;

use AppBundle\Components\DegMinSec;
use AppBundle\Components\Position;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('test')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $coords=[
            [
                [48.862725,2.287592],
                ["N48°51'45","E002°17'15"],
            ],
            [
                [-12.979173340519186,-38.51405832382716],
                ["S12°58'45","W038°30'50"],
            ],
        ];

        foreach($coords as $coord) {
            $output->writeln("-------------------------------------------------------");


            $output->writeln("decimal: ".$coord[0][0]." ".$coord[0][1]);
            $output->writeln("wanted: ".$coord[1][0]." ".$coord[1][1]);
            $output->writeln("result: ".(new DegMinSec($coord[0][0]))->format('lat')." ".(new DegMinSec($coord[0][1]))->format('long'));
        }

        $projection = $this->getContainer()->get('services.projection_service');

        $points = [
            [
                new Position(0, 0),
                new Position(34.26551518845619, 45.12949706032199),
                //"N45°07'46.189 E034°15'55.855",
                "N45°07'46 E034°15'55",
            ],
            [
                new Position(0, 500000),
                new Position(40.59116773673568, 44.88347735909489),
                //"N44°53'00.518 E040°35'28.204",
                "N44°53'00 E040°35'28",
            ],
            [
                new Position(-500000, 500000),
                new Position(40.06378105888497, 40.4174184154516),
                //"N40°25'02.706 E040°03'49.612",
                "N40°25'02 E040°03'49",
            ],
        ];

        foreach ($points as $point) {
            $pp = $projection->xyToLL($point[0]);

            $output->writeln("-------------------------------------------------------");
            $output->writeln((string)$point[0]);
            $output->writeln("wanted: ".$point[1]);
            $output->writeln("result: ".$pp);
            $output->writeln("wanted: ".$point[2]);
            $output->writeln("result: ".(new DegMinSec($pp->getY()))->format('lat')." ".(new DegMinSec($pp->getX()))->format('long'));
        }

    }

}
