<?php

namespace AppBundle\Controller;

use AppBundle\Lib\LuaParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/veaf/marker")
 */
class VeafMarkerController extends Controller
{

    private $heading = [0, 45, 90, 135, 180, 225, 270, 315];
    private $groundSpeed = [0, 5, 10, 15, 20, 25, 30];
    private $countries = ['values' => ['USA', 'Russia'], 'default' => 'Russia'];
    private $convoys = [
        'redconvoy-def' => 'RU supply convoy with defense',
        'redconvoy-lightdef' => 'RU supply convoy with light defense',
        'redconvoy-nodef' => 'RU supply convoy without defense',
        'redsmallconvoy-def' => 'RU small supply convoy with defense',
        'redsmallconvoy-lightdef' => 'RU small supply convoy with light defense',
        'redsmallconvoy-nodef' => 'RU small supply convoy with no defense',
        'blueconvoy' => 'US supply convoy',
    ];

    private $quantities = [
        'values' => [
            0 => 'no',
            1 => 'low',
            2 => 'medium',
            3 => 'high',
            4 => 'ultra',
        ],
        'default' => 1,
    ];

    private $sizes = [
        'values' => [
            1 => 'small',
            2 => 'medium',
            3 => 'big',
            4 => 'very big',
        ],
        'default' => 1,
    ];

    /**
     * @Route("/spawn/convoy", name="veaf_marker_convoy")
     *
     *
     * @Template()
     *
     * @return array
     */
    public function convoyAction(Request $request)
    {

        $options = [
            'convoy' => [
                'armor' => $this->quantities,
                'defense' => $this->quantities,
                'transport' => [
                    'values' => [
                        0 => 'no trucks',
                        2 => '2 trucks',
                        5 => '5 trucks',
                        7 => '7 trucks',
                        10 => '10 trucks',
                        15 => '15 trucks',
                        20 => '20 trucks',
                    ],
                    'default' => 5,
                ],
                'country' => $this->countries,
            ],
        ];

        return ['options' => $options];
    }

    /**
     * @Route("/spawn/cas-mission", name="veaf_marker_cas_mission")
     *
     *
     * @Template()
     *
     * @return array
     */
    public function casMissionAction(Request $request)
    {

        $options = [
            'cas' => [
                'armor' => $this->quantities,
                'defense' => $this->quantities,
                'size' => $this->sizes,
                'spacing' => [
                    'values' => [
                        0 => 'tight',
                        1 => 'normal',
                        2 => 'medium',
                        3 => 'high',
                        4 => 'ultra',
                    ],
                    'default' => 1,
                ],
            ],
        ];

        return ['options' => $options];
    }

    /**
     * @Route("/spawn/jtac", name="veaf_marker_jtac")
     *
     *
     * @Template()
     *
     * @return array
     */
    public function jtacAction(Request $request)
    {

        $options = [
            'jtac' => [
                'country' => [
                    'values' => ['USA', 'Russia'],
                    'default' => 'USA',
                ],
            ],
        ];

        return ['options' => $options];
    }

    /**
     * @Route("/spawn/{spawn}", name="veaf_marker_helper")
     *
     *
     * @Template()
     *
     * @return array
     */
    public function helperAction(Request $request, string $spawn = null)
    {

        $options = [
            'tanker' => [
                'name' => [
                    'values' => [
                        'SHELL',
                        'ARCO',
                        'TEXACO',
                    ],
                    'default' => 'SHELL',
                ],
                'heading' => [
                    'values' => $this->heading,
                    'default' => 0,
                ],
                'speed' => [
                    'values' => [
                        200,
                        225,
                        250,
                        275,
                        300,
                        325,
                        350,
                    ],
                    'default' => 300,
                ],
                'alt' => [
                    'values' => [
                        10000,
                        15000,
                        20000,
                        25000,
                    ],
                    'default' => 20000,
                ],
                'dist' => [
                    'values' => [
                        20,
                        30,
                        40,
                        50,
                    ],
                    'default' => 20,
                ],
            ],
            'move' => [
                'speed' => [
                    'values' => $this->groundSpeed,
                    'default' => 0,
                ],
            ],
            'smoke' => [
                'color' => [
                    'red',
                    'green',
                    'orange',
                    'blue',
                    'white',
                ],
            ],
            'flare' => [
                'alt' => [
                    1000,
                    2000,
                    3000,
                    4000,
                    5000,
                ],
            ],
            'cargo' => [
                'name' => [
                    'ammo' => 'ammo (~ 1500 lbs)',
                    'barrels' => 'barrels (~ 480 lbs)',
                    'container' => 'container (~ 1200 lbs)',
                    'fueltank' => 'fueltank (~ 2400 lbs)',
                    'f_bar' => 'f_bar (~ 823 lbs)',
                    'iso_container" selected="selected' => 'iso_container (~ 4500 lbs)',
                    'iso_container_small' => 'iso_container_small (~ 3200 lbs)',
                    'm117' => 'm117 (~ 840 lbs)',
                    'oiltank' => 'oiltank (~ 2300 lbs)',
                    'pipes_big' => 'pipes_big (~ 4815 lbs)',
                    'pipes_small' => 'pipes_small (~ 4350 lbs)',
                    'tetrapod' => 'tetrapod (~ 1500 lbs)',
                    'trunks_long' => 'trunks_long (~ 4747 lbs)',
                    'trunks_small' => 'trunks_small (~ 5000 lbs)',
                    'uh1h' => 'uh1h package (~ 1000 lbs)',

                    'jeep_cargo' => 'jeep_cargo (100 - 4000 kg)',
                    'bambi_bucket' => 'bambi_bucket (1000 - 2000 kg)',
                    'zu23_cargo' => 'zu23_cargo (1000 - 2000 kg)',
                    'blu82_cargo' => 'blu82_cargo (800 -5000 kg)',
                    'generator_cargo' => 'generator_cargo (1000 - 2000 kg)',
                    'Tschechenigel_cargo' => 'Tschechenigel_cargo (100 - 10000 kg)',
                    'sandbag' => 'sandbag (100 - 10000 kg)',
                    'booth_container' => 'booth_container (2200 - 10000 kg)',
                    'antenne' => 'antenne (2200 - 10000 kg)',
                    'mast' => 'mast (2200 - 10000 kg)',
                    'uh1_weapons' => 'uh1_weapons (100 - 10000 kg)',
                    'panzergranaten' => 'panzergranaten (1000 - 2000 kg)',
                    'pz2000_shell' => 'pz2000_shell (1000 - 2000 kg)',
                    'sandbag_box' => 'sandbag_box (1000 - 2000 kg)',
                    'fir_tree' => 'fir_tree (100 - 10000 kg)',
                    'hmvee_cargo' => 'hmvee_cargo (2200 - 10000 kg)',
                    'eurotainer' => 'eurotainer (800 - 5000 kg)',
                    'MK6' => 'MK6 (800 - 5000 kg)',
                    'concrete_pipe_duo' => 'concrete_pipe_duo (823 - 823 kg)',
                    'concrete_pipe' => 'concrete_pipe (823 - 823 kg)',
                    'gaz66_cargo' => 'gaz66_cargo (2200 - 10000 kg)',
                    'uaz_cargo' => 'uaz_cargo (2200 - 10000 kg)',
                    'stretcher_body' => 'stretcher_body (100 - 10000 kg)',
                    'stretcher_empty' => 'stretcher_empty (100 - 10000 kg)',
                    'san_container' => 'san_container (2200 - 10000 kg)',
                    'biwak_cargo' => 'biwak_cargo (2200 - 10000 kg)',
                    'wolf_cargo' => 'wolf_cargo (2200 - 10000 kg)',
                    'biwak_timber' => 'biwak_timber (100 - 480kg)',
                    'biwak_metal' => 'biwak_metal (100 - 480kg)',
                ],
            ],
            'unit' => [
                'name' => [
                    'Air Defense Units' => [
                        'sa9' => 'SA-9	Strela-1 9P31',
                        'sa13' => 'SA-13 Strela-10M3',
                        'sa6' => 'SA-6 Kub 2P25 launcher',
                        'sa8' => 'SA-8 Osa 9A33 launcher',
                        'sa15' => 'SA-15 Tor 9A331',
                        'shilka' => 'ZSU-23-4 Shilka',
                        'sa18' => 'SA-18 Igla-S manpad',
                    ],
                    'Tanks' => [
                        'Challenger2' => 'Challenger2',
                        'Leclerc' => 'Leclerc',
                        'Leopard1A3' => 'Leopard1A3',
                        'Leopard-2' => 'Leopard-2',
                        'M-60' => 'M-60',
                        'M1128 Stryker MGS' => 'M1128 Stryker MGS',
                        'M-1 Abrams' => 'M-1 Abrams',
                        'T-55' => 'T-55',
                        'T-72B' => 'T-72B',
                        'T-80UD' => 'T-80UD',
                        'T-90' => 'T-90',
                    ],
                    'Logistic' => [
                        'Ural-4320 APA-5D' => 'Ural-4320 APA-5D',
                        'ATMZ-5' => 'ATMZ-5',
                        'ATZ-10' => 'ATZ-10',
                        'GAZ-3307' => 'GAZ-3307',
                        'GAZ-3308' => 'GAZ-3308',
                        'GAZ-66' => 'GAZ-66',
                        'M978 HEMTT Tanker' => 'M978 HEMTT Tanker',
                        'HEMTT TFFT' => 'HEMTT TFFT',
                        'IKARUS Bus' => 'IKARUS Bus',
                        'KAMAZ Truck' => 'KAMAZ Truck',
                        'LAZ Bus' => 'LAZ Bus',
                        'Hummer' => 'Hummer',
                        'M 818' => 'M 818',
                        'MAZ-6303' => 'MAZ-6303',
                        'Predator GCS' => 'Predator GCS',
                        'Predator TrojanSpirit' => 'Predator TrojanSpirit',
                        'Suidae' => 'Suidae',
                        'Tigr_233036' => 'Tigr_233036',
                        'Trolley bus' => 'Trolley bus',
                        'UAZ-469' => 'UAZ-469',
                        'Ural ATsP-6' => 'Ural ATsP-6',
                        'Ural-375 PBU' => 'Ural-375 PBU',
                        'Ural-375' => 'Ural-375',
                        'Ural-4320-31' => 'Ural-4320-31',
                        'Ural-4320T' => 'Ural-4320T',
                        'VAZ Car' => 'VAZ Car',
                        'ZiL-131 APA-80' => 'ZiL-131 APA-80',
                        'SKP-11' => 'SKP-11',
                        'ZIL-131 KUNG' => 'ZIL-131 KUNG',
                        'ZIL-4331' => 'ZIL-4331',
                    ],
                ],
                'country' => $this->countries,
                'heading' => $this->heading,
                'speed' => $this->groundSpeed,
            ],
            'group' => [
                'name' => [
                    'Infantry' => [
                        'infsec' => 'Mechanized infantry section',
                    ],
                    'SAM Sites' => [
                        'sa2' => 'SA-2 SAM site',
                        'rapier_optical' => 'Rapier SAM site',
                        'rapier_radar' => 'Rapier SAM site with radar',
                        'sa3' => 'SA-3 SAM site',
                        'sa6' => 'SA-6 SAM site',
                        'sa10' => 'S300 SAM site',
                        'sa11' => 'SA-11 SAM site',
                        'roland' => 'Roland SAM site',
                        'hawk' => 'Hawk SAM site',
                        'patriot' => 'Patriot SAM site',
                    ],
                    'Air Defense' => [
                        'sa2' => 'SA-2',
                        'sa3' => 'SA-3',
                        'sa6' => 'SA-6',
                        'sa10' => 'SA-10',
                        'sa11' => 'SA-11',
                        'roland' => 'roland',
                        'rapier_optical' => 'SA-13 Strela-10M3',
                        'rapier_radar' => 'SA-6 Kub 2P25 launcher',
                        'hawk' => 'Hawk',
                        'sa15' => 'SA-15 Tor 9A331',
                        'shilka' => 'ZSU-23-4 Shilka',
                        'sa18' => 'SA-18 Igla-S manpad',
                    ],
                    'Convoy' => $this->convoys,
                ],
                'country' => $this->countries,
            ],
            'convoy' => [
                'armor' => $this->quantities,
                'defense' => $this->quantities,
                'transport' => [
                    'values' => [
                        0 => 'no trucks',
                        2 => '2 trucks',
                        5 => '5 trucks',
                        7 => '7 trucks',
                        10 => '10 trucks',
                        15 => '15 trucks',
                        20 => '20 trucks',
                    ],
                    'default' => 5,
                ],
                'country' => $this->countries,
            ],
        ];

        return ['options' => $options, 'spawn' => $spawn];
    }

}
