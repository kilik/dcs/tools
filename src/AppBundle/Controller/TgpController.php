<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Miz;
use AppBundle\Lib\LuaParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/tgp")
 */
class TgpController extends Controller
{
    /**
     * @Route("/test", name="tgp_test")
     *
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function testAction(Request $request)
    {

        return [
        ];
    }

}

