<?php

namespace AppBundle\Controller;

use AppBundle\Components\Dictionary;
use AppBundle\Components\Mission;
use AppBundle\Entity\Miz;
use AppBundle\Lib\LuaParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/miz")
 */
class MizController extends Controller
{
    /**
     * @Route("/import", name="miz_import")
     *
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function importAction(Request $request)
    {
        $form = $this->createFormBuilder()->add('file', FileType::class)->getForm();

        $form->handleRequest($request);

        $mission = null;

        if ($form->isSubmitted()) {
            // @todo
            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();
            try {
                $miz = $this->get("services.miz_service")->createFromMizFile($file->getRealPath());
                $miz->setTitle($file->getClientOriginalName());

                $this->getDoctrine()->getManager()->persist($miz);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('info', 'mission imported');

                return new RedirectResponse($this->generateUrl('miz_view', ['miz' => $miz->getUuid()]));
            } catch (\Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Export the json version
     *
     * @Route("/{miz}/export", name="miz_export")
     */
    public function exportAction(Miz $miz)
    {
        return new JsonResponse($miz->getOriginalMission());
    }

    /**
     * @Route("/{miz}/{side}", name="miz_view")
     *
     * @Template()
     *
     * @return array
     */
    public function viewAction(Miz $miz, $side = "mission_maker")
    {
        return [
            'miz' => $miz,
            'miz_service' => $this->get('services.miz_service'),
            'side' => $side,
        ];
    }

    /**
     * @Route("/{miz}/{side}/group/{groupId}", name="miz_view_group")
     *
     * @Template()
     *
     * @return array
     */
    public function viewGroupAction(Miz $miz, $side = "mission_maker", $groupId)
    {
        $mizService = $this->get('services.miz_service');

        return [
            'group' => $miz->getMission()->findGroupById($groupId),
            'miz' => $miz,
            'miz_service' => $mizService,
            'side' => $side,
        ];
    }

    /**
     * test
     *
     * @Route("/{miz}/test", name="miz_test")
     * @ParamConverter("miz", options={"mapping": {"miz" = "uuid"} })
     *
     * @Template()
     *
     * @return array
     */
    public function testAction(Miz $miz)
    {
        return [
            'miz' => $miz,
            'mission' => Mission::buildFromArray($miz->getOriginalMission(), Dictionary::buildFromArray($miz->getDictionary())),
        ];
    }

    /**
     *
     * @Template()
     */
    public function mapAction(Miz $miz, $side, $groupId)
    {
        $mizService = $this->get('services.miz_service');

        return [
            'group' => $miz->getMission()->findGroupById($groupId),
            'miz' => $miz,
            'miz_service' => $mizService,
            'side' => $side,
        ];
    }

}

