<?php

namespace AppBundle\Components;

trait ColorTrait
{

    /**
     * Color
     *
     * @var string
     */
    private $color = "#black;";

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * Set color from label
     *
     * @param string $composedLabel
     *
     * @return string the label without color part
     */
    public function setColorFromLabel(string $composedLabel)
    {
        $seperatorPos=strrpos($composedLabel,'#');

        if(false!==$seperatorPos) {
            $label=substr($composedLabel,0, $seperatorPos);
            $color=substr($composedLabel,$seperatorPos+1);

            $this->setColor($color);

            return $label;
        }
        else {
            return $composedLabel;
        }
    }
}