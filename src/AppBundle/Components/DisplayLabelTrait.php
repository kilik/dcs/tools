<?php

namespace AppBundle\Components;

trait DisplayLabelTrait
{

    /**
     * Display label ?
     *
     * @var bool
     */
    private $displayLabel = false;

    /**
     * @return bool
     */
    public function isDisplayLabel(): bool
    {
        return $this->displayLabel;
    }

    /**
     * @param bool $displayLabel
     */
    public function setDisplayLabel(bool $displayLabel)
    {
        $this->displayLabel = $displayLabel;
    }

    /**
     * Fill stroke attribute from label or name
     *
     * @param string $label
     */
    public function setDisplayLabelFromLabel(string $label)
    {
        $this->setDisplayLabel(strpos($label, "L") !== false);
    }
}