<?php

namespace AppBundle\Components;

class Waypoint implements BuildFromArrayInterface
{
    /**
     * Position
     *
     * @var Position
     */
    private $position;

    /**
     * Altitude
     *
     * @var int
     */
    private $altitude;

    /**
     * Speed
     *
     * @var float
     */
    private $speed;

    /**
     * eta
     *
     * @var string
     */
    private $eta;

    /**
     * name
     *
     * @var string
     */
    private $name;

    /**
     * action
     *
     * @var string
     */
    private $action;

    /**
     * type
     *
     * @var string
     */
    private $type;

    /**
     * Tasks
     *
     * @var Task[]
     */
    private $tasks = [];

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return int
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * @param int $altitude
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }

    /**
     * @return string
     */
    public function getEta()
    {
        return $this->eta;
    }

    /**
     * @param string $eta
     */
    public function setEta($eta)
    {
        $this->eta = $eta;
    }

    /**
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param float $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Position $position
     */
    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

    /**
     * @return Task[]
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param Task[] $tasks
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @param Task $task
     */
    public function addTask(Task $task)
    {
        $this->tasks[] = $task;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        /**
         *    1 => array:13 [▼
         *      "alt" => 720
         *      "action" => "From Ground Area"
         *      "alt_type" => "BARO"
         *      "speed" => 55.555555555556
         *      "task" => array:2 [▶]
         *      "type" => "TakeOffGround"
         *      "ETA" => 0
         *      "ETA_locked" => true
         *      "y" => 777606.62857144
         *      "x" => -296208.01714285
         *      "name" => "DictKey_WptName_133"
         *      "formation_template" => ""
         *      "speed_locked" => true
         *    ]
         */

        $data += [
            "ETA" => 0,
            "speed" => 0,
            "alt" => 0,
            "action" => "",
            "type" => "",
            "name" => "",
            "task" => ["params" => ["tasks" => []]],
        ];

        $waypoint = new Waypoint();

        $waypoint->setPosition(Position::buildFromArray($data, $dictionary));
        $waypoint->setEta($data['ETA']);
        $waypoint->setSpeed($data['speed']);
        $waypoint->setAltitude($data['alt']);
        $waypoint->setAction($data['action']);
        $waypoint->setType($data['type']);
        $waypoint->setName($dictionary->trans($data['name']));

        foreach ($data['task']['params']['tasks'] as $task) {
            $waypoint->addTask(Task::buildFromArray($task, $dictionary));
        }

        return $waypoint;
    }
}