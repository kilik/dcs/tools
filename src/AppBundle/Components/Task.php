<?php

namespace AppBundle\Components;

class Task implements BuildFromArrayInterface
{
    /**
     * Enabled
     *
     * @var bool
     */
    private $enabled;

    /**
     * Auto
     *
     * @var bool
     */
    private $auto;

    /**
     * Id
     *
     * @var string
     */
    private $id;

    /**
     * Number
     *
     * @var int
     */
    private $number;

    /**
     * Action
     *
     * @var Action
     */
    private $action;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return bool
     */
    public function isAuto()
    {
        return $this->auto;
    }

    /**
     * @param bool $auto
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param Action $action
     */
    public function setAction(Action $action)
    {
        $this->action = $action;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        /**
         * 1 => array:5 [▼
         * "enabled" => true
         * "auto" => true
         * "id" => "Tanker"
         * "number" => 1
         * "params" => []
         * ]
         * 2 => array:5 [▼
         * "enabled" => true
         * "auto" => true
         * "id" => "WrappedAction"
         * "number" => 2
         * "params" => array:1 [▼
         *   "action" => array:2 [▼
         *     "id" => "ActivateBeacon"
         *     "params" => array:7 [▼
         *       "type" => 4
         *       "frequency" => 1088000000
         *       "callsign" => "TKR"
         *       "channel" => 1
         *       "modeChannel" => "X"
         *       "bearing" => true
         *       "system" => 4
         *     ]
         *   ]
         * ]
         */

        $data += [
            "enabled" => false,
            "auto" => false,
            "id" => "",
            "number" => 1,
            "params" => [],
        ];
        $data['params'] += ["action" => []];

        $task = new Task();

        $task->setEnabled($data['enabled']);
        $task->setAuto($data['auto']);
        $task->setId($data['id']);
        $task->setNumber($data['number']);
        $task->setAction(Action::buildFromArray($data['params']['action'], $dictionary));

        return $task;
    }
}