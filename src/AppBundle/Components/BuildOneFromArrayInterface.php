<?php

namespace AppBundle\Components;

interface BuildOneFromArrayInterface
{

    /**
     * build one object from array
     *
     * @param array $data the original array mission
     * @param Dictionary $dictionary the original array dictionary
     *
     * @return object
     */
    public static function buildOneFromArray($data, Dictionary $dictionary);
}