<?php

namespace AppBundle\Components;

class Route implements BuildFromArrayInterface
{
    /**
     * Waypoint
     *
     * @var Waypoint[]
     */
    private $waypoints = [];

    /**
     * @return Waypoint[]
     */
    public function getWaypoints()
    {
        return $this->waypoints;
    }

    /**
     * @param Waypoint[] $waypoints
     */
    public function setWaypoints($waypoints)
    {
        $this->waypoints = $waypoints;
    }

    /**
     * @param Waypoint $waypoint
     */
    public function addWaypoint(Waypoint $waypoint)
    {
        $this->waypoints[] = $waypoint;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $route = new static();

        if (!isset($data['points'])) {
            throw new \InvalidArgumentException('points are missing');
        }

        foreach ($data['points'] as $point) {
            $route->addWaypoint(Waypoint::buildFromArray($point, $dictionary));
        }

        return $route;
    }

    /**
     * Find action by id
     *
     * @param string $id (ex: ActivateBeacon)
     * @return Action|null
     */
    public function findActionById($id)
    {
        if (is_array($this->getWaypoints())) {
            foreach ($this->getWaypoints() as $waypoint) {
                foreach ($waypoint->getTasks() as $task) {
                    if (null !== $task->getAction() && $task->getAction()->getId() == $id) {
                        return $task->getAction();
                    }
                }
            }
        }

        return null;
    }
}