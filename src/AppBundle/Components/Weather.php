<?php

namespace AppBundle\Components;

class Weather implements BuildFromArrayInterface
{
    /**
     * Name
     *
     * @var string
     */
    private $name;

    /**
     * QNH
     *
     * @var int
     */
    private $qnh;

    /**
     * Weather type
     *
     * @var int
     */
    private $type;

    /**
     * Winds
     *
     * @var Wind[]
     */
    private $winds;

    /**
     * @var float
     */
    private $temperature;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQnh()
    {
        return $this->qnh;
    }

    /**
     * @param int $qnh
     */
    public function setQnh($qnh)
    {
        $this->qnh = $qnh;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Wind[]
     */
    public function getWinds()
    {
        return $this->winds;
    }

    /**
     * @param Wind[] $winds
     */
    public function setWinds($winds)
    {
        $this->winds = $winds;
    }

    /**
     * @return float
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @param float $temperature
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        /**
         * "atmosphere_type" => 0
         * "wind" => array:3 [▼
         *      "at8000" => array:2 [▼
         *          "speed" => 4
         *          "dir" => 213
         *      ]
         *      "at2000" => array:2 [▼
         *          "speed" => 17
         *          "dir" => 258
         *      ]
         *      "atGround" => array:2 [▼
         *          "speed" => 6
         *          "dir" => 282
         *      ]
         * ]
         * "enable_fog" => false
         * "season" => array:1 [▼
         *      "temperature" => 19
         * ]
         * "type_weather" => 0
         * "qnh" => 761
         * "cyclones" => array:1 [▼
         *      1 => array:6 [▼
         *          "pressure_spread" => 829764.67419294
         *          "centerZ" => 288551.61898022
         *          "ellipticity" => 0.85621736471025
         *          "rotation" => -0.33307383604913
         *          "pressure_excess" => -1325
         *          "centerX" => -82319.836217066
         *      ]
         * ]
         * "name" => "Winter, clean sky"
         * "fog" => array:3 [▼
         *      "thickness" => 0
         *      "visibility" => 25
         *      "density" => 7
         * ]
         * "groundTurbulence" => 6
         * "visibility" => array:1 [▼
         *      "distance" => 80000
         * ]
         * "clouds" => array:4 [▼
         *      "thickness" => 762
         *      "density" => 10
         *      "base" => 2590
         *      "iprecptns" => 0
         * ]
         */
        $weather = new static();

        $data += [
            'name' => 'unknown',
            'qnh' => 1013,
            'type_weather' => 0,
            'season' => [],
        ];
        $data['season'] += ['temperature' => 0];

        $weather->setName($data['name']);
        $weather->setQnh($data['qnh']);
        $weather->setType($data['type_weather']);
        $weather->setTemperature($data['season']['temperature']);
        $weather->setWinds(Wind::buildFromArray($data['wind'], $dictionary));

        return $weather;
    }
}