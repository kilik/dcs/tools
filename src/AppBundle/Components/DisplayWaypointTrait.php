<?php

namespace AppBundle\Components;

trait DisplayWaypointTrait
{

    /**
     * Display waypoint ?
     *
     * @var bool
     */
    private $displayWaypoint = false;

    /**
     * @return bool
     */
    public function isDisplayWaypoint(): bool
    {
        return $this->displayWaypoint;
    }

    /**
     * @param bool $displayWaypoint
     */
    public function setDisplayWaypoint(bool $displayWaypoint)
    {
        $this->displayWaypoint = $displayWaypoint;
    }

    /**
     * Fill stroke attribute from label or name
     *
     * @param string $label
     */
    public function setDisplayWaypointFromLabel(string $label)
    {
        $this->setDisplayWaypoint(strpos($label, "W") !== false);
    }
}