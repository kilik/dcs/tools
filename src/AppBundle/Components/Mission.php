<?php

namespace AppBundle\Components;

class Mission implements BuildFromArrayInterface
{
    /**
     * Coalitions
     *
     * @var Coalition[]
     */
    private $coalitions = [];

    /**
     * Date
     *
     * @var \DateTime
     */
    private $date;

    /**
     * @var Weather
     */
    private $weather;

    /**
     * @var string
     */
    private $theatre;

    /**
     * @var Map
     */
    private $map;

    /**
     * @var int|string
     */
    private $version;

    /**
     * Trigger Zones
     *
     * @var TriggerZone[]
     */
    private $triggerZones = [];

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getTheatre()
    {
        return $this->theatre;
    }

    /**
     * @param string $theatre
     */
    public function setTheatre($theatre)
    {
        $this->theatre = $theatre;
    }

    /**
     * @return Coalition[]
     */
    public function getCoalitions()
    {
        return $this->coalitions;
    }

    /**
     * @param Coalition[] $coalitions
     */
    public function setCoalitions($coalitions)
    {
        $this->coalitions = $coalitions;
    }

    /**
     * Get Coalition by name
     *
     * @param string $name
     *
     * @return Coalition
     */
    public function getCoalition($name)
    {
        foreach ($this->coalitions as $coalition) {
            if ($coalition->getName() == $name) {
                return $coalition;
            }
        }
    }

    /**
     * @return TriggerZone[]
     */
    public function getTriggerZones(): array
    {
        return $this->triggerZones;
    }

    /**
     * @param TriggerZone[] $triggerZones
     */
    public function setTriggerZones(array $triggerZones)
    {
        $this->triggerZones = $triggerZones;
    }

    /**
     * @return Weather
     */
    public function getWeather()
    {
        return $this->weather;
    }

    /**
     * @param Weather $weather
     */
    public function setWeather($weather)
    {
        $this->weather = $weather;
    }

    /**
     * @return Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param Map $map
     */
    public function setMap($map)
    {
        $this->map = $map;
    }

    /**
     * @return int|string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int|string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $mission = new static();

        $data += [
            "theatre" => 'unknown',
            "version" => 'unknown',
            "triggers" => [],
        ];
        $data["triggers"] += ["zones" => []];

        // date
        if (!isset($data['date'])) {
            throw new \InvalidArgumentException('date is missing');
        }
        $mission->setDate(new \DateTime(sprintf('%04d-%02d-%02d', $data['date']['Year'], $data['date']['Month'], $data['date']['Day'])));
        $mission->setTheatre($data['theatre']);
        $mission->setVersion($data['version']);

        // wheater
        if (!isset($data['weather'])) {
            throw new \InvalidArgumentException('weather is missing');
        }
        $mission->setWeather(Weather::buildFromArray($data['weather'], $dictionary));

        // map
        if (!isset($data['map'])) {
            throw new \InvalidArgumentException('map is missing');
        }
        $mission->setMap(Map::buildFromArray($data['map'], $dictionary));

        // coalition
        if (!isset($data['coalition'])) {
            throw new \InvalidArgumentException('coalition is missing');
        }
        $mission->setCoalitions(Coalition::buildFromArray($data['coalition'], $dictionary));

        // triggers zone
        $mission->setTriggerZones(TriggerZone::buildManyFromArray($data['triggers']['zones'], $dictionary));

        return $mission;
    }

    /**
     * Alias - all groups
     *
     * @return AbstractGroup[]
     */
    public function getGroups()
    {
        $groups = [];

        foreach ($this->coalitions as $coalition) {
            foreach (array_merge($coalition->getPlanes(), $coalition->getHelicopters(), $coalition->getVehicles(), $coalition->getShips()) as $group) {
                $groups[] = $group;
            }
        }

        return $groups;
    }

    /**
     * Alias - all groups
     *
     * @param int $id
     * @return AbstractGroup
     */
    public function findGroupById($id)
    {
        foreach ($this->getGroups() as $group) {
            if ($group->getId() == $id) {
                return $group;
            }
        }

        return;
    }

}