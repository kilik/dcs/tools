<?php

namespace AppBundle\Components;

class Color implements BuildOneFromArrayInterface
{
    /**
     * Red
     *
     * @var float
     */
    private $r;

    /**
     * Green
     *
     * @var float
     */
    private $g;

    /**
     * Blue
     *
     * @var float
     */
    private $b;

    /**
     * Alpha
     *
     * @var float
     */
    private $a;

    /**
     * @return float
     */
    public function getA(): float
    {
        return $this->a;
    }

    /**
     * @param float $a
     */
    public function setA(float $a)
    {
        $this->a = $a;
    }

    /**
     * @return float
     */
    public function getB(): float
    {
        return $this->b;
    }

    /**
     * @param float $b
     */
    public function setB(float $b)
    {
        $this->b = $b;
    }

    /**
     * @return float
     */
    public function getG(): float
    {
        return $this->g;
    }

    /**
     * @param float $g
     */
    public function setG(float $g)
    {
        $this->g = $g;
    }

    /**
     * @return float
     */
    public function getR(): float
    {
        return $this->r;
    }

    /**
     * @param float $r
     */
    public function setR(float $r)
    {
        $this->r = $r;
    }

    /**
     * @inheritdoc
     */
    public static function buildOneFromArray($data, Dictionary $dictionary)
    {
        $color = new static();

        $data += [
            "r" => 0,
            "g" => 0,
            "b" => 0,
            "a" => 1,
        ];

        $color->r = $data['r'];
        $color->g = $data['g'];
        $color->b = $data['b'];
        $color->a = $data['a'];

        return $color;
    }

}