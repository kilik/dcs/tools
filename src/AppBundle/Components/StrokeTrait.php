<?php

namespace AppBundle\Components;

trait StrokeTrait
{

    /**
     * Stroke zone ?
     *
     * @var bool
     */
    private $stroke = false;

    /**
     * @return bool
     */
    public function isStroke(): bool
    {
        return $this->stroke;
    }

    /**
     * @param bool $stroke
     */
    public function setStroke(bool $stroke)
    {
        $this->stroke = $stroke;
    }

    /**
     * Fill stroke attribute from label or name
     *
     * @param string $label
     */
    public function setStrokeFromLabel(string $label)
    {
        $this->setStroke(strpos($label, "S") !== false);
    }
}