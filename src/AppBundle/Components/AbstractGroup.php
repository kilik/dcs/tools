<?php

namespace AppBundle\Components;

abstract class AbstractGroup implements BuildFromArrayInterface
{
    use DisplayLabelTrait;
    use DisplayWaypointTrait;
    use StrokeTrait;
    use ColorTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $frequency;

    /**
     * Position
     *
     * @var Position
     */
    private $position;

    /**
     * Route
     *
     * @var Route
     */
    private $route;

    /**
     * Task
     *
     * @var string
     */
    private $task;

    /**
     * Act as Tacan ?
     *
     * @var string
     */
    private $tacan;

    /**
     * Units
     *
     * @var Unit[]
     */
    private $units = [];

    /**
     * Coalition
     *
     * @var Coalition
     */
    private $coalition;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param int $frequency
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Position $position
     */
    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

    /**
     * @return Route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param Route $route
     */
    public function setRoute(Route $route)
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return Unit[]
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param Unit[] $units
     */
    public function setUnits($units)
    {
        $this->units = $units;
    }

    /**
     * @return string
     */
    public function getTacan()
    {
        return $this->tacan;
    }

    /**
     * @param string $tacan
     */
    public function setTacan($tacan = null)
    {
        $this->tacan = $tacan;
    }

    /**
     * @return Coalition
     */
    public function getCoalition()
    {
        return $this->coalition;
    }

    /**
     * @param Coalition $coalition
     */
    public function setCoalition(Coalition $coalition)
    {
        $this->coalition = $coalition;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $groups = [];

        foreach ($data as $dat) {
            $group = new static();

            $dat += [
                "groupId" => 0,
                "name" => 'unknown',
                "task" => 'unknown',
                "frequency" => null,
            ];

            $group->setId($dat['groupId']);
            $group->setPosition(Position::buildFromArray($dat, $dictionary));
            $group->setFrequency($dat['frequency']);
            $group->setTask($dat['task']);

            $label=$dictionary->trans($dat['name']);

            if (strpos($label, "_")) {
                $displayString = explode("_", $label);
                $group->setStrokeFromLabel($displayString[0]);
                $group->setDisplayWaypointFromLabel($displayString[0]);
                $group->setDisplayLabelFromLabel($displayString[0]);

                $label=$displayString[1];
            }

            $name=$group->setColorFromLabel($label);
            $group->setName($name);

            // route
            if (isset($dat['route'])) {
                $group->setRoute(Route::buildFromArray($dat['route'], $dictionary));
            }

            // units
            if (isset($dat['units'])) {
                $group->setUnits(Unit::buildFromArray($dat['units'], $dictionary));
            }

            // second pass:
            // find the first TACAN and set it
            if (null !== $group->getRoute()) {
                $actionTacan = $group->getRoute()->findActionById('ActivateBeacon');
                if (null !== $actionTacan) {
                    $group->setTacan(sprintf('%02d', $actionTacan->getChannel()).$actionTacan->getModeChannel());
                }
            }

            $groups[] = $group;
        }

        return $groups;
    }
}