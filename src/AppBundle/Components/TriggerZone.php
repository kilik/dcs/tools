<?php

namespace AppBundle\Components;

class TriggerZone implements BuildOneFromArrayInterface, BuildManyFromArrayInterface
{
    use StrokeTrait;

    /**
     * Position
     *
     * @var Position
     */
    private $position;

    /**
     * Radius (in meters ?)
     *
     * @var int
     */
    private $radius;

    /**
     * Color
     *
     * @var Color
     */
    private $color;

    /**
     * Name
     *
     * @var string
     */
    private $name;

    /**
     * Hidden
     *
     * @var bool
     */
    private $hidden;

    /**
     * Display label ?
     *
     * @var bool
     */
    private $displayLabel = false;

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @param Color $color
     */
    public function setColor(Color $color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getRadius(): int
    {
        return $this->radius;
    }

    /**
     * @param int $radius
     */
    public function setRadius(int $radius)
    {
        $this->radius = $radius;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Position $position
     */
    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return bool
     */
    public function isDisplayLabel(): bool
    {
        return $this->displayLabel;
    }

    /**
     * @param bool $displayLabel
     */
    public function setDisplayLabel(bool $displayLabel)
    {
        $this->displayLabel = $displayLabel;
    }


    /**
     * @inheritdoc
     */
    public static function buildOneFromArray($data, Dictionary $dictionary)
    {
        /**
         * triggers": {
         *  "zones": {
         *   "1": {
         *    "y": -5889.2034285713,
         *    "radius": 24384,
         *    "zoneId": 51,
         *    "color": {
         *      "1": 1,
         *      "2": 1,
         *      "3": 1,
         *      "4": 0.15
         *    },
         *    "x": 109524.142,
         *    "hidden": false,
         *    "name": "SAM NES"
         *   }
         *  }
         * },
         */

        $data += [
            "x" => 0,
            "y" => 0,
            "radius" => 0,
            "color" => [],
            "hidden" => false,
            "name" => "noname",
        ];

        $zone = new static();
        $zone->setColor(Color::buildOneFromArray($data['color'], $dictionary));
        $zone->setPosition(Position::buildFromArray($data, $dictionary));
        $zone->setRadius($data['radius']);
        $zone->setHidden($data['hidden']);

        if (strpos($data['name'], "_")) {
            $string = explode("_", $data['name']);

            $zone->setStrokeFromLabel($string[0]);
            $zone->setName($string[1]);
        }
        else {
            $zone->setName($data['name']);
        }

        return $zone;
    }

    /**
     * @inheritdoc
     */
    public static function buildManyFromArray($data, Dictionary $dictionary)
    {
        $zones = [];
        foreach ($data as $dat) {
            $zones[] = static::buildOneFromArray($dat, $dictionary);
        }

        return $zones;
    }

}