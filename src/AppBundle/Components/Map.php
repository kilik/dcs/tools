<?php

namespace AppBundle\Components;

class Map implements  BuildFromArrayInterface
{
    /**
     * @var int
     */
    private $centerX;

    /**
     * @var int
     */
    private $centerY;


    /**
     * @return int
     */
    public function getCenterX()
    {
        return $this->centerX;
    }

    /**
     * @param int $centerX
     */
    public function setCenterX($centerX)
    {
        $this->centerX = $centerX;
    }

    /**
     * @return int
     */
    public function getCenterY()
    {
        return $this->centerY;
    }

    /**
     * @param int $centerY
     */
    public function setCenterY($centerY)
    {
        $this->centerY = $centerY;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $map = new static();

        $data += [
            "centerX" => 0,
            "centerY" => 0,
        ];

        $map->setCenterX($data['centerX']);
        $map->setCenterY($data['centerY']);

        return $map;
    }
}