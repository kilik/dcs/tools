<?php

namespace AppBundle\Components;

class Coalition implements BuildFromArrayInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * Bullseye position
     *
     * @var Position
     */
    private $bullseye;

    /**
     * Helicopters
     *
     * @var HelicopterGroup[]
     */
    private $helicopters = [];

    /**
     * Planes
     *
     * @var PlaneGroup[]
     */
    private $planes = [];

    /**
     * Vehicles
     *
     * @var VehicleGroup[]
     */
    private $vehicles = [];

    /**
     * Ships
     *
     * @var ShipGroup[]
     */
    private $ships = [];

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Position
     */
    public function getBullseye()
    {
        return $this->bullseye;
    }

    /**
     * @param Position $bullseye
     */
    public function setBullseye($bullseye)
    {
        $this->bullseye = $bullseye;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return HelicopterGroup[]
     */
    public function getHelicopters()
    {
        return $this->helicopters;
    }

    /**
     * @param HelicopterGroup[] $helicopters
     */
    public function addHelicopters($helicopters)
    {
        $this->helicopters = array_merge($this->helicopters, $helicopters);
    }

    /**
     * @return VehicleGroup[]
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * @param VehicleGroup[] $vehicles
     */
    public function addVehicles($vehicles)
    {
        $this->vehicles = array_merge($this->vehicles, $vehicles);
    }

    /**
     * @return PlaneGroup[]
     */
    public function getPlanes()
    {
        return $this->planes;
    }

    /**
     * @param PlaneGroup[] $planes
     */
    public function addPlanes($planes)
    {
        $this->planes = array_merge($this->planes, $planes);
    }


    /**
     * @return ShipGroup[]
     */
    public function getShips()
    {
        return $this->ships;
    }

    /**
     * @param ShipGroup[] $ships
     */
    public function addShips($ships)
    {
        $this->ships = array_merge($this->ships, $ships);
    }

    /**
     * @return AbstractGroup[]
     */
    public function getAllGroups()
    {
        return array_merge($this->getPlanes(), $this->getHelicopters(), $this->getVehicles(), $this->getShips());
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $coalitions = [];

        foreach ($data as $coalitionCode => $dat) {
            $coalition = new static();
            $coalition->setCode($coalitionCode);

            $dat += [
                "name" => 'unknown',
                'bullseye' => [],
            ];

            $dat['bullseye'] += [
                'x' => 0,
                'y' => 0,
            ];

            $coalition->setName($dat['name']);
            $coalition->setBullseye(Position::buildFromArray($dat['bullseye'], $dictionary));

            // countries
            if (!isset($dat['country'])) {
                throw new \InvalidArgumentException('country is missing');
            }

            foreach ($dat['country'] as $country) {
                if (isset($country['vehicle']) && isset($country['vehicle']['group'])) {
                    $coalition->addVehicles(VehicleGroup::buildFromArray($country['vehicle']['group'], $dictionary));
                }
                if (isset($country['helicopter']) && isset($country['helicopter']['group'])) {
                    $coalition->addHelicopters(HelicopterGroup::buildFromArray($country['helicopter']['group'], $dictionary));
                }
                if (isset($country['plane']) && isset($country['plane']['group'])) {
                    $coalition->addPlanes(PlaneGroup::buildFromArray($country['plane']['group'], $dictionary));
                }
                if (isset($country['ship']) && isset($country['ship']['group'])) {
                    $coalition->addShips(ShipGroup::buildFromArray($country['ship']['group'], $dictionary));
                }
            }

            foreach ($coalition->getAllGroups() as $group) {
                $group->setCoalition($coalition);
            }

            $coalitions[] = $coalition;
        }

        return $coalitions;
    }
}