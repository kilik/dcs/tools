<?php

namespace AppBundle\Components;

class Position implements BuildFromArrayInterface
{
    /**
     * @var int
     */
    private $x;

    /**
     * @var int
     */
    private $y;

    /**
     * Position constructor.
     * @param $x
     * @param $y
     */
    public function __construct($x = 0, $y = 0)
    {
        $this->x = $x;
        $this->y = $y;
    }


    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $position = new static();

        $data += [
            "x" => 0,
            "y" => 0,
        ];

        $position->setX($data['x']);
        $position->setY($data['y']);

        return $position;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '('.$this->getX().';'.$this->getY().')';
    }
}