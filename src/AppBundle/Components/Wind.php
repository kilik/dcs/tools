<?php

namespace AppBundle\Components;


class Wind implements BuildFromArrayInterface
{
    const ALTITUDES = [
        "at8000" => 8000,
        "at2000" => 2000,
        "atGround" => 0,
    ];

    /**
     * Altitude
     *
     * @var int
     */
    private $altitude;

    /**
     * Speed
     *
     * @var int
     */
    private $speed;

    /**
     * Direction
     *
     * @var int
     */
    private $direction;

    /**
     * @return int
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * @param int $altitude
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }

    /**
     * @param string $code
     */
    public function setAltitudeCoded($code)
    {
        if (isset($this::ALTITUDES[$code])) {
            $this->altitude = $this::ALTITUDES[$code];
        } else {
            throw new \InvalidArgumentException('altitude code '.$code.' is unknown');
        }
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param int $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        /**
         *      "at8000" => array:2 [▼
         *          "speed" => 4
         *          "dir" => 213
         *      ]
         *      "at2000" => array:2 [▼
         *          "speed" => 17
         *          "dir" => 258
         *      ]
         *      "atGround" => array:2 [▼
         *          "speed" => 6
         *          "dir" => 282
         *      ]
         */
        $winds = [];

        foreach ($data as $altitudeCoded => $dat) {
            $wind = new static();

            $dat += [
                'speed' => 0,
                'dir' => 0,
            ];

            $wind->setSpeed($dat['speed']);
            $wind->setDirection($dat['dir']);
            $wind->setAltitudeCoded($altitudeCoded);
            $winds[] = $wind;
        }

        // sort winds by altitude
        usort($winds,function($a,$b) {
           return $a->getAltitude()>=$b->getAltitude();
        });

        return $winds;
    }
}