<?php

namespace AppBundle\Components;

/**
 * @deprecated use BuildOneFromArrayInterface or BuildManyFromArrayInterface instead
 */
interface BuildFromArrayInterface
{

    /**
     * build from array
     *
     * @param array $data the original array mission
     * @param Dictionary $dictionary the original array dictionary
     *
     * @return Mission
     */
    public static function buildFromArray($data, Dictionary $dictionary);
}