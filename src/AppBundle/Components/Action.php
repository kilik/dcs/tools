<?php

namespace AppBundle\Components;

class Action implements BuildFromArrayInterface
{
    /**
     * Id
     *
     * @var string
     */
    private $id;

    /**
     * Callsign
     *
     * @var string
     */
    private $callsign;

    /**
     * Channel
     *
     * @var string
     */
    private $channel;

    /**
     * mode channel
     *
     * @var string
     */
    private $modeChannel;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCallsign()
    {
        return $this->callsign;
    }

    /**
     * @param string $callsign
     */
    public function setCallsign($callsign)
    {
        $this->callsign = $callsign;
    }

    /**
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    public function getModeChannel()
    {
        return $this->modeChannel;
    }

    /**
     * @param string $modeChannel
     */
    public function setModeChannel($modeChannel)
    {
        $this->modeChannel = $modeChannel;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        /**
         *     "id" => "ActivateBeacon"
         *     "params" => array:7 [▼
         *       "type" => 4
         *       "frequency" => 1088000000
         *       "callsign" => "TKR"
         *       "channel" => 1
         *       "modeChannel" => "X"
         *       "bearing" => true
         *       "system" => 4
         *     ]
         */

        $data += [
            "id" => "",
            "params" => [],
        ];
        $data['params'] +=
            [
                "type" => null,
                "frequency" => null,
                "callsign" => null,
                "channel" => null,
                "modeChannel" => null,
                "bearing" => null,
                "system" => null,
            ];

        $action = new Action();

        $action->setId($data['id']);
        $action->setCallsign($data['params']['callsign']);
        $action->setChannel($data['params']['channel']);
        $action->setModeChannel($data['params']['modeChannel']);

        return $action;
    }
}