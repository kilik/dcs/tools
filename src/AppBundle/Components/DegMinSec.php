<?php

namespace AppBundle\Components;

class DegMinSec
{
    /**
     * @var int
     */
    private $sign;

    /**
     * @var int
     */
    private $deg;

    /**
     * @var int
     */
    private $min;

    /**
     * @var float
     */
    private $sec;

    /**
     * DegMinSec constructor.
     *
     * @param float $decimal
     */
    public function __construct($decimal)
    {
        $this->sign = ($decimal >= 0 ? 1 : -1);
        $this->deg = floor(abs($decimal));
        $rest = abs($decimal) - $this->deg;
        $seconds = 3600 * $rest;
        $this->min = floor($seconds / 60);
        $this->sec = $seconds - 60 * $this->min;
    }

    /**
     * @return int
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * @return int
     */
    public function getDeg()
    {
        return $this->deg;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @return float
     */
    public function getSec()
    {
        return $this->sec;
    }

    /**
     * @param string $type 'lat'|long'
     *
     * @return string
     */
    public function format($type)
    {
        if ($type == 'lat') {
            return sprintf("%s%02d°%02d'%02d", $this->sign > 0 ? 'N' : 'S', $this->deg, $this->min, $this->sec);
        } elseif ($type == 'long') {
            return sprintf("%s%03d°%02d'%02d", $this->sign > 0 ? 'E' : 'W', $this->deg, $this->min, $this->sec);
        } else {
            throw new \InvalidArgumentException('unknown type '.$type);
        }
    }

}