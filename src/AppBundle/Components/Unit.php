<?php

namespace AppBundle\Components;


class Unit implements BuildFromArrayInterface
{

    /**
     * Position
     *
     * @var Position
     */
    private $position;

    /**
     * Type
     *
     * @var string
     */
    private $type;

    /**
     * Name
     *
     * @var string
     */
    private $name;

    /**
     * Heading
     *
     * @var float
     */
    private $heading;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Position $position
     */
    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * @param float $heading
     */
    public function setHeading($heading)
    {
        $this->heading = $heading;
    }

    /**
     * @inheritdoc
     */
    public static function buildFromArray($data, Dictionary $dictionary)
    {
        $units = [];

        foreach ($data as $dat) {
            $unit = new static();

            $dat += [
                'type' => '',
                'name' => '',
                'heading' => 0,
            ];

            $unit->setName($dictionary->trans($dat['name']));
            $unit->setType($dat['type']);
            $unit->setHeading($dat['heading']);
            $unit->setPosition(Position::buildFromArray($dat, $dictionary));

            $units[] = $unit;
        }

        return $units;
    }
}