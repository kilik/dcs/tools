<?php

namespace AppBundle\Components;

class Dictionary
{
    /**
     * @var array
     */
    private $words;

    /**
     * @param array $data
     * @return static
     */
    public static function buildFromArray($data)
    {
        $dictionary = new static();
        $dictionary->words = $data;

        return $dictionary;
    }

    /**
     * Translate
     *
     * @param string $key
     * @return string
     */
    public function trans($key)
    {
        if (!isset($this->words[$key])) {
            return $key;
        }

        return $this->words[$key];
    }
}