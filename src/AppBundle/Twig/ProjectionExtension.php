<?php

namespace AppBundle\Twig;

use AppBundle\Components\Color;
use AppBundle\Components\DegMinSec;
use AppBundle\Components\Position;
use AppBundle\Services\ProjectionService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

use proj4php\Proj4php;
use proj4php\Proj;
use proj4php\Point;

class ProjectionExtension extends AbstractExtension
{

    /**
     * @var ProjectionService
     */
    private $projection;

    /**
     * ProjectionExtension constructor.
     * @param ProjectionService $projection
     */
    public function __construct(ProjectionService $projection)
    {
        $this->projection = $projection;
    }

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return array(
            new TwigFilter('xyToLat', array($this, 'xyToLat')),
            new TwigFilter('xyToLong', array($this, 'xyToLong')),
            new TwigFilter('xyToLL', array($this, 'xyToLL')),
            new TwigFilter('latDec', array($this, 'latDec')),
            new TwigFilter('latLongDec', array($this, 'latLongDec')),
            new TwigFilter('longDec', array($this, 'longDec')),
            new TwigFilter('rgbaJs', array($this, 'rgbaJs')),
        );
    }

    /**
     * @param Position $p
     * @param string $map
     *
     * @return Position
     */
    public function xyToLL(Position $p, $map = "caucasus")
    {
        return $this->projection->xyToLL($p, $map);
    }

    /**
     * @param Position $p
     * @param string $map
     *
     * @return float
     */
    public function xyToLat(Position $p, $map = "caucasus")
    {
        return ($this->projection->xyToLL($p, $map))->getY();
    }

    /**
     * @param Position $p
     * @param string $map
     *
     * @return float
     */
    public function xyToLong(Position $p, $map = "caucasus")
    {
        return ($this->projection->xyToLL($p, $map))->getX();
    }

    /**
     * @param Position $p
     *
     * @return string
     */
    public function latDec(Position $p)
    {
        return (new DegMinSec($p->getY()))->format('lat');
    }

    /**
     * @param Position $p
     *
     * @return string
     */
    public function latLongDec(Position $p)
    {
        return (new DegMinSec($p->getY()))->format('lat')." ".(new DegMinSec($p->getX()))->format('long');
    }

    /**
     * @param Position $p
     *
     * @return string
     */
    public function longDec(Position $p)
    {
        return (new DegMinSec($p->getX()))->format('long');
    }

    /**
     * Convert Color to rgba js (open layer)
     *
     * @param Color $c
     *
     * @return string
     */
    public function rgbaJs(Color $c)
    {
        return sprintf("rgba(%d,%d,%d,%f)", 255 * $c->getR(), 255 * $c->getG(), 255 * $c->getB(), $c->getA());
    }
}