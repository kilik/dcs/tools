<?php

namespace AppBundle\Request\ParamConverter;

use AppBundle\Entity\Miz;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MizConverter implements ParamConverterInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * MizConverter constructor.
     *
     * @param RegistryInterface $em
     */
    public function __construct(RegistryInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $converter)
    {
        $uuid = $request->get($converter->getName());
        if (null === $uuid && !$converter->isOptional()) {
            throw new \InvalidArgumentException('parameter '.$converter->getName().' is mandatory');
        }
        $side=$request->get('side','mission_maker');

        switch ($side) {
            case "blue":
                $field = "blueUuid";
                break;
            case "red":
                $field = "redUuid";
                break;
            case "mission_maker":
            default:
                $field = "uuid";
                break;

        }
        $miz = $this->em->getRepository('AppBundle:Miz')->findOneBy([$field => $uuid]);
        if(null===$miz) {
            throw new NotFoundHttpException();
        }
        $miz->setSide($side);
        $request->attributes->set($converter->getName(), $miz);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $converter)
    {
        return $converter->getClass() == Miz::class;
    }
}