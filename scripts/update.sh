#!/bin/bash

# switch the application in maintenance mod
#./bin/console lexik:maintenance:lock -n

# versionning update
git pull
composer install
#bower install

# apply migrations
./bin/console doctrine:migrations:migrate --no-interaction

# clear the cache
./bin/console cache:clear --env=prod

# and switch back the application in production mode
#./bin/console lexik:maintenance:unlock -n
